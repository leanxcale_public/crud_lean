var JDBC = require('jdbc');
var jinst = require('jdbc/lib/jinst');

if (!jinst.isJvmCreated()){
  jinst.addOption("-Xrs");
  jinst.setupClasspath(['./lib/qe-driver-0.400-20200121.154643-28-jdbc-client.jar']);
}

var conf = {
  url:'jdbc:leanxcale://localhost:1529/APP',
  drivername: 'com.leanxcale.client.Driver',
  properties: {
    user:'JESUS',
    password: 'JESUS'
  }
};

var lx = new JDBC(conf);
//initialize the connection
lx.initialize(function(err) {
  if (err) {
    console.log(err);
  } else{
    console.log('Connection initialized without error')
  }
});

module.exports = lx;
