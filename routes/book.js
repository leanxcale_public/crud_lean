var express = require('express');
var router = express.Router();
var asyncjs = require('async');
var lx = require('../connection.js');
const GET_ALL_BOOKS = 'SELECT * FROM JESUS.LEANTABLE';
const GET_SINGLE_BOOK_BY_ID = 'SELECT * FROM JESUS.LEANTABLE WHERE ISBN = ?';
const SAVE_BOOK = 'INSERT INTO JESUS.LEANTABLE values (?, ?, ?, ?, ?, CURRENT_DATE)';
const UPDATE_BOOK = 'UPDATE JESUS.LEANTABLE SET TITLE=?, AUTHOR=?, PUBLISHED_YEAR=?, PUBLISHER=?, UPDATED_DATE=CURRENT_DATE WHERE ISBN = ?';
const DELETE_BOOK = 'DELETE FROM JESUS.LEANTABLE WHERE ISBN = ?';

/* GET ALL BOOKS */
router.get('/', function(req, res, next) {
  lx.reserve(function(err, connObj) {
    console.log('Using connection ' + connObj.conn);
    var conn = connObj.conn;
    asyncjs.series([
        function(callback) {
          conn.createStatement(function (err, statement) {
            if (err) {
              console.log(err);
              callback(err);
            } else {
              statement.executeQuery(GET_ALL_BOOKS, function (err, resultset) {
                if (err) {
                  console.error(err);
                  callback(err);
                } else {
                  resultset.toObjArray(function(err, results) {
                    if (results.length > 0) {
                      console.log(results);
                      res.json(results);
                    }
                    callback(null, resultset);
                  });
                }
              });
            }
          })
        }
      ], function(err, results) {
        lx.release(connObj, function(err){
          if(err){
            console.log(err);
          }
        });
      }
    )
  });
});

/* GET SINGLE BOOK BY ID */
router.get('/:id', function(req, res, next) {
  lx.reserve(function(err, connObj) {
    console.log('Using connection ' + connObj.conn);
    var conn = connObj.conn;
    asyncjs.series([
        function(callback) {
          conn.prepareStatement(GET_SINGLE_BOOK_BY_ID,function (err, statement) {
            if (err) {
              console.log(err);
              callback(err);
            } else {
              statement.setString(1,req.params.id, function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });
              statement.executeQuery(function (err, resultset) {
                if (err) {
                  console.error(err);
                  callback(err);
                } else {
                  resultset.toObjArray(function(err, results) {
                    if (results.length > 0) {
                      console.log(results);
                    }
                    res.json(results);
                    callback(null, resultset);
                  });
                }
              });
            }
          })
        }
      ], function(err, results) {
        lx.release(connObj, function(err){
          if(err){
            console.log(err);
          }
        });
      }
    )
  });
});

/* SAVE BOOK */
router.post('/', function(req, res, next) {
  lx.reserve(function(err, connObj) {
    console.log('Using connection ' + connObj.conn);
    var conn = connObj.conn;
    asyncjs.series([
      function (callback) {
        conn.setAutoCommit(true, function (err) {
          if (err) {
            callback(err);
          } else {
            callback(null);
          }
        });
      },
    ], function (err, results) {
    });
    asyncjs.series([
        function(callback) {
          conn.prepareStatement(SAVE_BOOK, function (err, statement) {
            if (err) {
              console.log(err);
              callback(err);
            } else {
              statement.setString(1,req.body.ISBN, function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });
              statement.setString(2,req.body.TITLE, function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });
              statement.setString(3,req.body.AUTHOR, function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });

              statement.setString(4,req.body.PUBLISHED_YEAR.toString(), function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });
              statement.setString(5,req.body.PUBLISHER, function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });

              statement.executeUpdate(function (err, resultset) {
                  if (err) {
                    console.error(err);
                    callback(err);
                  } else {
                    res.json(req.body);
                    callback(null, resultset)
                  }
                });
            }
          })
        }
      ], function(err, results) {
        lx.release(connObj, function(err){
          if(err){
            console.log(err);
          }
        });
      }
    )
  });
});

/* UPDATE BOOK */
router.put('/:id', function(req, res, next) {
  lx.reserve(function(err, connObj) {
    console.log('Using connection ' + connObj.conn);
    var conn = connObj.conn;
    asyncjs.series([
      function (callback) {
        conn.setAutoCommit(true, function (err) {
          if (err) {
            callback(err);
          } else {
            callback(null);
          }
        });
      },
    ], function (err, results) {
    });
    asyncjs.series([
        function(callback) {
          conn.prepareStatement(UPDATE_BOOK, function (err, statement) {
            if (err) {
              console.log(err);
              callback(err);
            } else {
              statement.setString(5, req.body.ISBN, function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });
              statement.setString(1, req.body.TITLE, function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });
              statement.setString(2, req.body.AUTHOR, function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });
              statement.setString(3, req.body.PUBLISHED_YEAR.toString(), function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });
              statement.setString(4, req.body.PUBLISHER, function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });
              statement.executeUpdate(function (err, resultset) {
                if (err) {
                  console.error(err);
                  callback(err);
                } else {
                  res.json(req.body);
                  callback(null, resultset)
                }
              });
            }
          })
        }
      ], function(err, results) {
        lx.release(connObj, function(err){
          if(err){
            console.log(err);
          }
        });
      }
    )
  });
});

/* DELETE BOOK */
router.delete('/:id', function(req, res, next) {
  lx.reserve(function(err, connObj) {
    console.log('Using connection ' + connObj.conn);
    var conn = connObj.conn;
    asyncjs.series([
      function (callback) {
        conn.setAutoCommit(true, function (err) {
          if (err) {
            callback(err);
          } else {
            callback(null);
          }
        });
      },
    ], function (err, results) {
    });
    asyncjs.series([
        function(callback) {
          conn.prepareStatement(DELETE_BOOK,function (err, statement) {
            if (err) {
              console.log(err);
              callback(err);
            } else {
              statement.setString(1,req.params.id, function(err){
                if(err){
                  console.log(err);
                  callback(err);
                }
              });
              statement.executeQuery(function (err, resultset) {
                if (err) {
                  console.error(err);
                  callback(err);
                } else {
                  res.json(resultset);
                  callback(null,resultset);
                }
              });
            }
          });
        }
      ], function(err, results) {
        lx.release(connObj, function(err){
          if(err){
            console.log(err);
          }
        });
      }
    )
  });
});

module.exports = router;
