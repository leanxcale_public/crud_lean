import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BookCreateComponent implements OnInit {

  book = {
    UPDATED_DATE: undefined,
    PUBLISHED_YEAR: undefined,
    PUBLISHER: undefined,
    AUTHOR: undefined,
    TITLE: undefined,
    ISBN: undefined,
    DESCRIPTION: undefined
  };

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  saveBook() {
    this.book.UPDATED_DATE=new Date();
    this.book.DESCRIPTION="TBD";
    this.http.post('http://localhost:3000/book', this.book)
      .subscribe(res => {
          let id = res['ISBN'];
          this.router.navigate(['/book-details', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
